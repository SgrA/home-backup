#!/bin/bash

xrdb -merge ~/.Xresources

i3-msg "workspace 1; append_layout ~/.i3/workspace_1.json"

(urxvt &)
(urxvt -e su - &)
(urxvt &)

sleep 0.5
i3-msg "workspace 2; exec /usr/bin/chromium"
sleep 1.5
i3-msg "workspace 1"
